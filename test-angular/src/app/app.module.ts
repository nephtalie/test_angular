import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router'
import { AppComponent } from './app.component';
import { AppHeaderComponent } from './app-header/app-header.component';
import { PostListComponent } from './post-list/post-list.component';
import { PostFormComponent } from './post-form/post-form.component';
import { ReactiveFormsModule, FormGroup} from '@angular/forms';
import { UserService } from './user.service';
import { PostService } from './post.service';
import { HttpClientModule} from '@angular/common/http';

const appRoutes: Routes = [
  {path : '', component:PostListComponent },
  {path: 'edit',component: PostFormComponent },
  {path: 'edit/:id',component: PostFormComponent }
]



@NgModule({
  declarations: [
    AppComponent,
    AppHeaderComponent,
    PostListComponent,
    PostFormComponent,
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  exports:[RouterModule],
  providers: [UserService,PostService],
  bootstrap: [AppComponent]
})
export class AppModule { }
