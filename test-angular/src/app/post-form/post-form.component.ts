import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PostService } from '../post.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs';
import { UserService } from '../user.service';

interface Post{
  userId:number;
  id:number;
  title:string; 
  body:string;
  username?:string;
}

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})

export class PostFormComponent implements OnInit , OnDestroy{
  
 
editForm : FormGroup;
postList = [];
id : number; 
currentPost = {} as Post;
postSubscription = new Subscription();


  constructor(private postService:PostService, private route : ActivatedRoute, private userService : UserService) { 
    
  }

  ngOnInit() {

// au lieu de form control formBuilder
    this.editForm = new FormGroup({
        'title' : new FormControl(null,[Validators.required, Validators.maxLength(100), Validators.minLength(5)],  ),
        'username' : new FormControl(null),
        'userID' : new FormControl(null, [Validators.required]),
        'body': new FormControl(null, [Validators.required])
    });

    this.route.params.subscribe((params : Params) =>{
        
      this.id= +params['id']+1;
        
        if(this.id){
          this.postSubscription = this.postService.fetchPost(this.id).subscribe(post =>{ 
          this.currentPost = post;



          this.userService.fetchUsers().subscribe(users =>{
            
            /*Affichage du contenu pour un post existant.
       
       dans ng onInit, on initialise la valeur du formControl à null. Lorsqu'on charge un post existant
       de la liste de post, on repase par ngOnInit et donc la valeur est nul malgré qu'elle soit affiché dans 
       l'input. J'appel donc la création d'un "différent" formulaire au moment où on confirme l'existance du 
       post en question afin de pouvoir passer la valeur au FormControl. 
       */
          this.editForm = new FormGroup({
            'title' : new FormControl(this.currentPost.title ,[Validators.required, Validators.maxLength(100), Validators.minLength(5)],  ),
            'username' : new FormControl(users[this.currentPost.userId -1].name),
            'userID' : new FormControl(this.currentPost.userId, [Validators.required]),
            'body': new FormControl(this.currentPost.body, [Validators.required])});
      
        });
      });
    }
    
  }
     
  );
  }

  



  onSubmit(){
    console.log("Formulaire soumis")
  }

  ngOnDestroy(): void {
    this.postSubscription.unsubscribe();
  }
  
}
