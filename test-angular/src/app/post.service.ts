import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {map} from 'rxjs/operators'





@Injectable()
export class PostService{
loadedPosts = [];
    
    constructor(private http:HttpClient){}

    ngOnInit(){
     
    }

 
    
     fetchPosts(){
        return this.http.get('https://jsonplaceholder.typicode.com/posts').pipe(
            map(responseData=>{

           const postsArray =[];
           for (const key in responseData){
            if(responseData.hasOwnProperty(key)){
                postsArray.push({...responseData[key], id: key})
            }
        }
           return postsArray;
        
        }));

    }


    fetchPost(post):{[s:string]:any}{
        //retourne un objet de type Post
        return this.http.get('https://jsonplaceholder.typicode.com/posts/' + post);
    }

    fetchUserPost(userId){
        // retourne un array de posts 

        return this.http.get<Post[]>('https://jsonplaceholder.typicode.com/posts?userId='+userId);

        
    }

    

}