import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';




@Injectable()
export class UserService{

    constructor(private http: HttpClient){
        
    }

fetchUsers(){
//returns an array of user objects
    return this.http.get<User[]>('https://jsonplaceholder.typicode.com/users'); 
}


}