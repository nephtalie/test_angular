import { Component, OnInit, OnDestroy } from '@angular/core';
import { PostService } from '../post.service';
import { UserService } from '../user.service';
import {  Subscription } from 'rxjs';


@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit, OnDestroy {
postList = [];
users: User[] = [];
selectedUserPosts:Post[] =[];
postSubscription = new Subscription();
userSubscription = new Subscription();



  
constructor( private postService:PostService, private userService : UserService) { 

}

  ngOnInit() {
    //this.postList = this.postService.loadedPosts; 
   this.postSubscription = this.postService.fetchPosts().subscribe(posts =>{ 
    this.postList = posts;
} 
);

this.userSubscription = this.userService.fetchUsers().subscribe(users =>{
this.users = users;
});

  }

  onModifyclick(postID){
     console.log(postID);
  }

  onSelectionChange(selectedUser){
    
    // Si la sélection est "select a user " on ne place pas la requête 
    if(selectedUser.target.value != 0 ){
      
      this.postSubscription =  this.postService.fetchUserPost(selectedUser.target.value).subscribe(posts =>{
      this.selectedUserPosts = posts;

    });
   }
    

  }


  ngOnDestroy(){
    this.userSubscription.unsubscribe();
    this.postSubscription.unsubscribe();
  }

}
